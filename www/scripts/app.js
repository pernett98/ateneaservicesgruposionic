// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ion-floating-menu'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      .state('login', {
        cache: false,
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl as loginVm'
      })


      .state('cursos', {
        url: '/cursos',
        templateUrl: "templates/cursos.html",
        controller: "CursosCtrl as cursosVm"
      })

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
      })

      // Each tab has its own nav history stack:


      .state('tab.foro', {
        url: '/foro',
        views: {
          'tab-foro': {
            templateUrl: 'templates/tab-foro.html',
            controller: 'ForoCtrl'
          }
        }
      })

      .state('tab.contenido', {
        url: '/contenido',
        views: {
          'tab-contenido': {
            templateUrl: 'templates/tab-contenido.html',
            controller: 'ContenidoCtrl as contenidoVm'
          }
        }
      })
      .state('tab.contenido-nuevo', {
        url: '/contenido/nuevo',
        views: {
          'tab-contenido': {
            templateUrl: 'templates/contenido-form.html',
            controller: 'ContenidoCtrl as contenidoVm'
          }
        }
      })
      .state('tab.contenido-detail', {
        url: '/contenido/:contenidoId',
        views: {
          'tab-contenido': {
            templateUrl: 'templates/contenido-detail.html',
            controller: 'ContenidoDetailCtrl'
          }
        }
      })

      .state('tab.account', {
        url: '/account',
        views: {
          'tab-account': {
            templateUrl: 'templates/tab-account.html',
            controller: 'AccountCtrl'
          }
        }
      })

      .state('tab.eventos', {
        url: '/eventos',
        views: {
          'tab-eventos': {
            templateUrl: 'templates/tab-eventos.html',
            controller: 'EventosCtrl as eventosVm'
          }
        }
      })
      .state('tab.eventos-nuevo', {
        url: '/eventos/nuevo',
        views: {
          'tab-eventos': {
            templateUrl: 'templates/eventos-form.html',
            controller: 'EventosCtrl as eventosVm'
          }
        }
      })
      .state('tab.eventos-detalles', {
        url: '/eventos/:ideventos',
        views: {
          'tab-eventos': {
            templateUrl: 'templates/eventos-detalles.html',
            controller: 'EventosCtrl as eventosVm'
          }
        }
      })

      .state('tab.tareas', {
        url: '/tareas',
        views: {
          'tab-tareas': {
            templateUrl: 'templates/tab-tareas.html',
            controller: 'TareasCtrl as tareasVm'
          }
        }
      })
      .state('tab.tareas-nuevo', {
        url: '/tareas/nuevo',
        views: {
          'tab-tareas': {
            templateUrl: 'templates/mesa-form.html',
            controller: 'TareasCtrl as tareasVm'
          }
        }
      })
      .state('tab.tareas-detalles', {
        url: '/tareas/:idtareas',
        views: {
          'tab-tareas': {
            templateUrl: 'templates/tareas-detalles.html',
            controller: 'TareasCtrl as tareasVm'
          }
        }
      })

      // .state('tab.puntos-entrega', {
      //   url: '/puntos-entrega',
      //   views: {
      //     'tab-puntos-entrega': {
      //       templateUrl: 'templates/tab-puntos-entrega.html',
      //       controller: 'PuntosEntregaCtrl as puntosEntegaVm'
      //     }
      //   }
      // })
      // .state('tab.punto-entrega-nuevo', {
      //   url: '/punto-entrega/nuevo',
      //   views: {
      //     'tab-puntos-entrega': {
      //       templateUrl: 'templates/punto-entrega-form.html',
      //       controller: 'PuntosEntregaCtrl as puntosEntegaVm'
      //     }
      //   }
      // })
      // .state('tab.punto-entrega-detalles', {
      //   url: '/punto-entrega/:idPuntoEntrega',
      //   views: {
      //     'tab-puntos-entrega': {
      //       templateUrl: 'templates/punto-entrega-detalles.html',
      //       controller: 'PuntosEntregaCtrl as puntosEntegaVm'
      //     }
      //   }
      // })

      .state('tab.config', {
        url: '/config',
        views: {
          'tab-config': {
            templateUrl: 'templates/tab-config.html',
            controller: 'ConfigCtrl as configVm'
          }
        }
      })

      .state('tab.perfiles', {
        url: '/perfiles',
        views: {
          'tab-config': {
            templateUrl: 'templates/tab-perfiles.html',
            controller: 'PerfilesCtrl as perfilesVm'
          }
        }
      })
      .state('tab.perfil-nuevo', {
        url: '/perfiles/nuevo',
        views: {
          'tab-config': {
            templateUrl: 'templates/perfil-form.html',
            controller: 'PerfilesCtrl as perfilesVm'
          }
        }
      })
      .state('tab.perfil-detalles', {
        url: '/punto-entrega/:idPerfil',
        views: {
          'tab-config': {
            templateUrl: 'templates/perfil-detalles.html',
            controller: 'PerfilesCtrl as perfilesVm'
          }
        }
      })

      .state('tab.usuarios', {
        url: '/usuarios',
        views: {
          'tab-usuarios': {
            templateUrl: 'templates/tab-usuarios.html',
            controller: 'UsuariosCtrl as usuariosVm'
          }
        }
      })
      .state('tab.usuarios-nuevo', {
        url: '/usuarios/nuevo',
        views: {
          'tab-usuarios': {
            templateUrl: 'templates/usuario-form.html',
            controller: 'UsuariosCtrl as usuariosVm'
          }
        }
      })
      .state('tab.usuario-detalles', {
        url: '/usuarios/:idUsuario',
        views: {
          'tab-usuarios': {
            templateUrl: 'templates/usuario-detalles.html',
            controller: 'UsuariosCtrl as usuariosVm'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

  });
