(function() {
  'use strict';

  angular
    .module('starter.services')
    .service('ServiceProductos', ServiceProductos);

  ServiceProductos.$inject = ['$http'];

  /* @ngInject */
  function ServiceProductos($http) {
    this.getProductos = getProductos;
    this.createProducto = createProducto;

    function getProductos() {
      return [];
    }

    function createProducto(product) {
      //create producto
    }
  }
})();
