(function () {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('ContenidoCtrl', ContenidoCtrl);

  ContenidoCtrl.$inject = ['$scope', 'FactoryContenidos', '$state', '$ionicModal'];

  function ContenidoCtrl($scope, FactoryContenidos, $state, $ionicModal) {
    var vm = this;
    vm.contenidos = FactoryContenidos.contenidos;
    vm.newContent = newContent;

    vm.form = {
      name: '',
      content: ''
    };

    $ionicModal.fromTemplateUrl('templates/contenido-form.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.contenidoModal = modal;
    });


    ////////////////

    function newContent() {
      FactoryContenidos.createContenido(this.form);
      vm.closeContenidoModal();
    }

    vm.openContenidoModal = function() {
      $scope.contenidoModal.show();
    };
    vm.closeContenidoModal = function() {
      $scope.contenidoModal.hide();
      vm.form = {
        name: '',
        content: ''
      };
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.contenidoModal.remove();
    });

  }
})();
