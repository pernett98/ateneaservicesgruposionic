(function () {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['factoryUsuarios', '$state', '$ionicPopup'];

  /* @ngInject */
  function LoginCtrl(factoryUsuarios, $state, $ionicPopup) {
    var vm = this;
    vm.form = {};
    vm.login = login;

    function login() {
      var res = factoryUsuarios.loginUsuario(vm.form);
      if (res) {
        $state.go('cursos');
      } else {
        alert('Error, usuario y/o contraseña incorrectos');
      }
    }
  }
})();
