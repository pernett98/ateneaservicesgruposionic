(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('TareasCtrl', TareasCtrl);

  TareasCtrl.$inject = ['$scope', 'FactoryTareas'];
  function TareasCtrl($scope, FactoryTareas) {
    var vm = this;
    vm.tareas = FactoryTareas.tareas;

    activate();

    ////////////////

    function activate() { }
  }
})();
