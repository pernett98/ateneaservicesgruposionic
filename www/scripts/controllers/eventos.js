(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('EventosCtrl', EventosCtrl);

  EventosCtrl.$inject = ['$scope', 'FactoryEventos'];

  function EventosCtrl($scope, FactoryEventos) {
    var vm = this;
    vm.eventos = FactoryEventos.eventos;

    activate();

    ////////////////

    function activate() { }
  }
})();
