(function () {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('CursosCtrl', CursosCtrl);

  CursosCtrl.$inject = ['$state', 'FactoryCursos', '$ionicNavBarDelegate'];

  /* @ngInject */
  function CursosCtrl($state, FactoryCursos, $ionicNavBarDelegate) {
    var vm = this;
    vm.form = {};

    $ionicNavBarDelegate.showBackButton(false);

    vm.cursos = FactoryCursos.cursos;

  }
})();
