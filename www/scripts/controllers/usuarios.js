(function () {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('UsuariosCtrl', UsuariosCtrl);

  UsuariosCtrl.$inject = ['$scope', 'factoryUsuarios', '$state', '$ionicModal'];

  /* @ngInject */
  function UsuariosCtrl($scope, factoryUsuarios, $state, $ionicModal) {
    var vm = this;
    vm.form = {
      id: undefined,
      nombre: "",
      apellidos: "",
      email: "",
      telefono: ""
    };

    factoryUsuarios.getUsuarios();
    vm.factoryUsuarios = factoryUsuarios;
    vm.usuarios = vm.factoryUsuarios.usuarios;
    vm.createUsuario = createUsuario;
    vm.newUsuario = newUsuario;
    vm.disableUsuario = disableUsuario;
    vm.updateUsuario = updateUsuario;
    vm.justLetters = justLetters;
    vm.openUpdateUsuario = openUpdateUsuario;

    $ionicModal.fromTemplateUrl('templates/usuario-form.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.usuarioFormModal = modal;
    });

    vm.openUsuarioFormModal = function () {
      $scope.usuarioFormModal.show();
    };

    vm.closeUsuarioFormModal = function () {
      $scope.usuarioFormModal.hide();
      vm.form = {
        id: undefined,
        nombre: "",
        apellidos: "",
        email: "",
        telefono: ""
      };
    };

    $ionicModal.fromTemplateUrl('templates/usuario-detalles.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.usuarioDetallesFormModal = modal;
    });

    vm.openUsuarioDetallesFormModal = function () {
      $scope.usuarioDetallesFormModal.show();
    };

    vm.closeUsuarioDetallesFormModal = function () {
      $scope.usuarioDetallesFormModal.hide();
      vm.form = {
        id: undefined,
        nombre: "",
        apellidos: "",
        email: "",
        telefono: ""
      };
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.contenidoModal.remove();
    });

    function createUsuario() {
      // esta funcion dirige la app a la vista del formulario
      // $state.go('tab.usuarios-nuevo');
      vm.openUsuarioFormModal();
    }

    function newUsuario() {
      factoryUsuarios.createUsuario(vm.form).then(function () {
        vm.closeUsuarioFormModal();
        // $state.go('tab.usuarios');
      });
    }

    function disableUsuario(idUsuario) {
      factoryUsuarios.disableUsuario(idUsuario);
    }

    // if ($state.params.idUsuario) {
    //   vm.form = factoryUsuarios.getUsuarioById($state.params.idUsuario);
    // }

    function openUpdateUsuario(usuario) {
      vm.form = usuario;
      vm.openUsuarioDetallesFormModal();

    }

    function updateUsuario() {
      factoryUsuarios.updateUsuario(vm.form);
      // $state.go('tab.usuarios', {});
      vm.closeUsuarioDetallesFormModal();
    }

    function justLetters() {
      if ((event.keyCode < 97 || event.keyCode > 122) && (event.keyCode < 65 || event.keyCode > 90)) {
        event.returnValue = false;
      }
    }
  }

})();
