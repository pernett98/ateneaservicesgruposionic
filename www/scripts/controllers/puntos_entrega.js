(function() {
    'use strict';

    angular
        .module('starter.controllers')
        .controller('PuntosEntregaCtrl', PuntosEntregaCtrl);

    PuntosEntregaCtrl.$inject = ['factoryPuntosEntrega', '$state'];

    /* @ngInject */
    function PuntosEntregaCtrl(factoryPuntosEntrega, $state) {
        var vm = this;
        vm.form = {};
        vm.puntosEntrega = [];

        factoryPuntosEntrega.getPuntosEntrega();
        vm.factoryPuntosEntrega = factoryPuntosEntrega;
        vm.puntosEntrega = vm.factoryPuntosEntrega.puntosEntrega;
        vm.createPuntoEntrega = createPuntoEntrega;
        vm.newPuntoEntrega = newPuntoEntrega;
        vm.updatePuntoEntrega = updatePuntoEntrega;
        vm.disablePuntoEntrega = disablePuntoEntrega;

        function createPuntoEntrega() {
          $state.go('tab.punto-entrega-nuevo');
        }

        function newPuntoEntrega() {
          factoryPuntosEntrega.createPuntoEntrega(vm.form);
          $state.go('tab.puntos-entrega');
        }

        function updatePuntoEntrega() {
          factoryPuntosEntrega.updatePuntoEntrega(vm.form);
          $state.go('tab.puntos-entrega', {});
        }

        function disablePuntoEntrega(idProducto) {
          factoryPuntosEntrega.disablePuntoEntrega(idProducto);
        }

        if ($state.params.idPuntoEntrega) {
          vm.form = factoryPuntosEntrega.getPuntoEntregaById($state.params.idPuntoEntrega);
        }
    }
})();
