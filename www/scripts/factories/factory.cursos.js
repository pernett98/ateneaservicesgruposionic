(function () {
  'use strict';

  angular
    .module('starter.services')
    .factory('FactoryCursos', FactoryCursos);

  // FactoryCursos.$inject = [];

  function FactoryCursos() {
    var service = {
      cursos: [{
        name: 'Curso Excel Principiante',
        img: './images/curso1.jpg',
      }, {
        name: 'Curso Excel Intermedio',
        img: './images/curso2.jpg',
      }, {
        name: 'Curso Excel Avanzado',
        img: './images/curso1.jpg',
      }],
      exposedFn: exposedFn
    };

    return service;

    ////////////////
    function exposedFn() {}
  }
})();
