(function () {
  'use strict';

  angular
    .module('starter.services')
    .factory('FactoryEventos', FactoryEventos);

  // FactoryEventos.$inject = [''];

  function FactoryEventos() {
    var date = new Date();

    var service = {
      eventos: [
        {
          name: 'Evento 1',
          dateStart: new Date (date.getDate() -2)
        },
        {
          name: 'Evento 2',
          dateStart: new Date (date.getDate() -2)
        }
      ]
    };

    return service;

    ////////////////
    function exposedFn() {}
  }
})();
