(function() {
    'use strict';

    angular
        .module('starter.services')
        .factory('factoryMesas', factoryMesas);

    // factoryMesas.$inject = [''];

    /* @ngInject */
    function factoryMesas() {
        var service = {
            getMesas: getMesas,
            getMesaById: getMesaById,
            createMesa: createMesa,
            updateMesa: updateMesa,
            disableMesa: disableMesa,
            counter: 3,
            mesas: [
              {
                id: '01',
                nombre: 'mesa 1',
                isDisabled: false
              },
              {
                id: '02',
                nombre: 'mesa 2',
                isDisabled: false
              }
            ]
        };

        return service;

        function getMesas() {

        }

        function getMesaById(idMesa) {
          return service.mesas.find(function (mesa) {
            return mesa.id == idMesa;
          });
        }

        function createMesa(form) {
          form.id = service.counter++;
          form.isDisabled = false;
          service.mesas.push(form);
        }

        function updateMesa(form) {
          service.mesas.find(function (mesa) {
            if (mesa.id == form.id) {
              mesa = form;
            }
          });
        }

        function disableMesa(idMesa) {
          service.mesas.find(function (mesa) {
            if (mesa.id == idMesa) {
              mesa.isDisabled = !mesa.isDisabled;
            }
          });
        }

    }
})();
