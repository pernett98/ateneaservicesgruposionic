(function () {
  'use strict';

  angular
    .module('starter.services')
    .factory('FactoryContenidos', FactoryContenidos);

  // FactoryContenidos.$inject = [''];
  function FactoryContenidos() {
    var service = {
      createContenido: createContenido,
      contenidos: [{
          name: 'Contenio 1',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere risus tortor, in vestibulum purus tristique nec. Donec fermentum consectetur ipsum, eu porttitor ex egestas interdum. Cras quis nunc fermentum tellus elementum iaculis sed et enim. Nam ullamcorper lacus a ex tincidunt, ac interdum massa eleifend. Ut enim nisi, dignissim at lectus sit amet, viverra pharetra lectus.'
        },
        {
          name: 'Contenio 2',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere risus tortor, in vestibulum purus tristique nec. Donec fermentum consectetur ipsum, eu porttitor ex egestas interdum. Cras quis nunc fermentum tellus elementum iaculis sed et enim. Nam ullamcorper lacus a ex tincidunt, ac interdum massa eleifend. Ut enim nisi, dignissim at lectus sit amet, viverra pharetra lectus.'
        }
      ]
    };

    return service;

    ////////////////
    function createContenido(data) {
      service.contenidos.unshift(data);
    }
  }
})();
