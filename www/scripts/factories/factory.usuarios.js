(function () {
  'use strict';

  angular
    .module('starter.services')
    .factory('factoryUsuarios', factoryUsuarios);

  factoryUsuarios.$inject = ['ServiceUsuarios', '$q', '$rootScope'];

  /* @ngInject */
  function factoryUsuarios(ServiceUsuarios, $q, $rootScope) {
    var service = {
      getUsuarios: getUsuarios,
      getUsuarioById: getUsuarioById,
      getUsuarioByEmail: getUsuarioByEmail,
      createUsuario: createUsuario,
      updateUsuario: updateUsuario,
      disableUsuario: disableUsuario,
      loginUsuario: loginUsuario,

      counter: 3, //variable temporal para simular los ids
      usuarios: [{
          id: 980307,
          nombre: 'Tatiana',
          apellidos: 'Zapata Cano',
          email: 'tatiana_zapata23151@elpoli.edu.co',
          telefono: '2863617',
          img: './images/ionic.png',
          isDisabled: false,
          password: '123',
          isAdmin: true,
        }, {
          id: 980604,
          nombre: 'Sebastian',
          apellidos: 'Pernett Plaza',
          email: 'sebastian@elpoli.edu.co',
          telefono: '9475723',
          img: './images/ben.png',
          isDisabled: false,
          password: '123',
          isAdmin: true,
        }, {
          id: 9804,
          nombre: 'Miguel',
          apellidos: 'Sánchez Rios',
          email: 'migue@elpoli.edu.co',
          telefono: '9475723',
          img: './images/ben.png',
          isDisabled: false,
          password: '123',
          isAdmin: true,
        }, {
          id: 980113,
          nombre: 'Iván Andrés',
          apellidos: 'Gutiérrez Agudelo',
          email: 'ivan_gutierrez23151@elpoli.edu.co',
          telefono: '1332351',
          img: './images/perry.png',
          isDisabled: false,
          password: '123',
          isAdmin: true,
        },
        {
          id: 0,
          nombre: 'user',
          apellidos: 'Gutiérrez Agudelo',
          email: 'user@mail.com',
          telefono: '1332351',
          img: './images/max.png',
          isDisabled: false,
          password: '123',
          isAdmin: false,
        }
      ],
      userLogged: {},
    };

    return service;

    function getUsuarios() {
      // ServiceUsuarios.getUsuarios().then(function (result) {
      //   service.usuarios = result.data;
      // }, function (result) {
      //   alert(result.messege);
      // });
    }

    function getUsuarioById(idUsuario) {
      return service.usuarios.find(function (usuario) {
        return usuario.id == idUsuario;
      });
    }

    function getUsuarioByEmail(email) {
      return service.usuarios.find(function (usuario) {
        return usuario.email === email;
      });
    }

    function createUsuario(form) {
      var deferred = $q.defer();

      if (!getUsuarioById(form.id)) {
        if (!getUsuarioByEmail(form.email)) {
          //form.id = service.counter++;
          form.img = './images/user.png';
          form.isDisabled = false;
          form.email = form.email.toLowerCase();
          form.isAdmin = false;
          //ServiceProductos.createProducto(form);
          service.usuarios.push(form);
          // getProductos();
          deferred.resolve();
        } else {
          deferred.reject();
          alert("Ya existe un usuario con ese email");
        }
      } else {
        deferred.reject();
        alert("Ya existe un usuario con ese DNI");
      }

      return deferred.promise;
    }

    function disableUsuario(idUsuario) {
      service.usuarios.find(function (usuario) {
        if (usuario.id == idUsuario) {
          usuario.isDisabled = !usuario.isDisabled;
        }
      });
    }

    function updateUsuario(form) {
      service.usuarios.find(function (usuario) {
        if (usuario.id == form.id) {
          if (usuario.email == form.email) {
            usuario = form;
          } else {
            alert("Ya existe un usuario con ese email");
          }
        }
      });
    }

    function loginUsuario(form) {
      var res;
      var email = form.email.toLowerCase();
      var password = form.password;

      service.usuarios.find(function (usuario) {
        if (usuario.email == email) {
          if (usuario.password == password) {
            res = true;
            service.userLogged = usuario;
            $rootScope.userLogged = usuario;
            return true;
          }
        }
      });

      return res;
    }

    getUsuarios();
  }
})();
