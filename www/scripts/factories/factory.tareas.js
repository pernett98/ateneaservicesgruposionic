(function() {
  'use strict';

  angular
    .module('starter.services')
    .factory('FactoryTareas', FactoryTareas);

  //FactoryTareas.$inject = [''];
  function FactoryTareas() {
    var service = {
      exposedFn:exposedFn,
      tareas: [
        {
          name: "tarea 1",
          description: "Nulla facilisi. Duis feugiat, libero a pharetra ornare,",
        },
        {
          name: "tarea 2",
          description: "Nulla facilisi. Duis feugiat, libero a pharetra ornare,",
        },
      ]
    };

    return service;

    ////////////////
    function exposedFn() { }
  }
})();
